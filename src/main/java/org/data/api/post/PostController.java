package org.data.api.post;


import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.data.api.annotations.JwtUser;
import org.data.api.post.request.AddPostRequest;
import org.data.api.post.request.UpdatePostRequest;
import org.data.api.post.responce.PostResponce;
import org.data.api.user.User;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

import static org.data.api.utils.ApiUtil.AUTHORIZATION;

@Slf4j
@RestController
@RequestMapping("/api/v1/posts")
@AllArgsConstructor
public class PostController {

  private final PostService postService;

  @SecurityRequirement(name = AUTHORIZATION)
  @PostMapping(path = "/add")
  public PostResponce addPost(@JwtUser User user,
                              @RequestBody AddPostRequest request) {

    Post post = new Post();
    post.setUserId(user.getId());
    post.setText(request.getText());
    post.setCreationDate(new Date());
    post.setUpdateDate(new Date());

    return postService.addPost(post);
  }


  @SecurityRequirement(name = AUTHORIZATION)
  @PostMapping(path = "/update")
  public Map<String, Object> updatePost(@JwtUser User user,
                                        @RequestBody UpdatePostRequest request) {
    try {
      PostResponce responce = postService.updatePost(user.getId(), request);
      return Map.of(
        "status", "ok",
        "data", responce
      );
    } catch (Exception e) {
      log.error("updatePost: ", e);
      return Map.of(
        "status", "error"
      );
    }
  }

  @SecurityRequirement(name = AUTHORIZATION)
  @DeleteMapping(path = "/delete")
  public Map<String, String> deletePost(@RequestParam int id, @JwtUser User user) {
    try {
      postService.delete(id, user.getId());
      return Map.of(
        "status", "ok"
      );
    } catch (Exception e) {
      log.error("error invalid user!!", e);
      return Map.of("status", "error"
      );
    }
  }


  @GetMapping(path = "/list")
  public Map<String, Object> list(@RequestParam int userId,
                                  @RequestParam int offset,
                                  @RequestParam int limit,
                                  @RequestParam(defaultValue = "desc") String order) {
    return postService.list(userId, offset, limit, order);
  }
}




