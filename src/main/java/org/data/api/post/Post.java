package org.data.api.post;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Post {

  private Integer id;
  private Integer userId;
  private String text;
  private Date creationDate;
  private Date updateDate;
}
