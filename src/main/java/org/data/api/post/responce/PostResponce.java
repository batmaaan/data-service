package org.data.api.post.responce;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.data.api.post.Post;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostResponce {
  private Integer id;
  private Integer userId;
  private String text;
  private Date creationDate;
  private Date updateDate;

  public static PostResponce fromPost(Post post) {
    return PostResponce.builder()
      .id(post.getId())
      .userId(post.getUserId())
      .text(post.getText())
      .creationDate(post.getCreationDate())
      .updateDate(post.getUpdateDate())
      .build();
  }
}
