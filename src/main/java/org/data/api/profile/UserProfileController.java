package org.data.api.profile;


import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;
import org.data.api.annotations.JwtUser;
import org.data.api.jwt.JwtService;
import org.data.api.profile.request.UpdateProfileRequest;
import org.data.api.profile.response.UserProfileResponse;
import org.data.api.user.User;
import org.springframework.web.bind.annotation.*;

import static org.data.api.utils.ApiUtil.AUTHORIZATION;

@Slf4j
@RestController
@RequestMapping("/api/v1/users/profile")
public class UserProfileController {

  private final UserProfileService userProfileService;

  public UserProfileController(UserProfileService userProfileService) {

    this.userProfileService = userProfileService;
  }

  @SecurityRequirement(name = AUTHORIZATION)
  @GetMapping("/me")
  public UserProfileResponse getProfile(@JwtUser User user) {
    UserProfile profileByUserId = userProfileService.getProfileByUserId(user.getId());
    return UserProfileResponse.fromProfile(profileByUserId);
  }

  @SecurityRequirement(name = AUTHORIZATION)
  @PostMapping("/update")
  public UserProfileResponse updateProfile(@RequestBody UpdateProfileRequest profileRequest, @JwtUser User user) {
    if (profileRequest != null) {
      return userProfileService.update(user.getId(), profileRequest);
    } else {
      log.warn("такого юзера нет");
    }
    return null;
  }
}
