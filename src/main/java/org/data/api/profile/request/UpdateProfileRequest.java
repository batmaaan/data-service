package org.data.api.profile.request;

import lombok.*;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateProfileRequest {
  private String nickname;
  private String about;
  private Date birthday;
}

