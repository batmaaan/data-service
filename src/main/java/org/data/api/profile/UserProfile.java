package org.data.api.profile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProfile {
  private Integer id;
  private Integer userId;
  @Size(min = 5, max = 30, message = "Max symbols - 30, min symbols - 5")
  private String nickname;
  private Date birthday;
  private String about;

}
