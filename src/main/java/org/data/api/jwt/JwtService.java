package org.data.api.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.JWTVerifier;
import lombok.extern.slf4j.Slf4j;
import org.data.api.user.User;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Slf4j
@Service
public class JwtService {
  private static final Algorithm ALGORITHM_HS = Algorithm.HMAC256("secret");
  private static final JWTVerifier JWT_VERIFIER = JWT
    .require(ALGORITHM_HS)
    .acceptExpiresAt(Instant.now().plus(5, ChronoUnit.HOURS).toEpochMilli())
    .build();

  public String encode(User user) {
    return JWT.create()
      .withClaim("id", user.getId())
      .sign(ALGORITHM_HS);
  }

  public User decode(String jwtDecodeToken) {


    if (jwtDecodeToken == null || jwtDecodeToken.isEmpty()) {
      return null;
    }
    var decodedJWT = JWT_VERIFIER.verify(jwtDecodeToken);
    Integer id = decodedJWT.getClaim("id").asInt();
    if (id != null) {
      User user = new User();
      user.setId(id);
      return user;
    } else {
      return null;
    }

  }

}
