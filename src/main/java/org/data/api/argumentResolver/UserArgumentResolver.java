package org.data.api.argumentResolver;

import lombok.AllArgsConstructor;
import org.data.api.annotations.JwtUser;
import org.data.api.jwt.JwtService;
import org.data.api.user.User;
import org.springframework.core.MethodParameter;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import static org.data.api.utils.ApiUtil.AUTHORIZATION;


@Component
@AllArgsConstructor
public class UserArgumentResolver implements HandlerMethodArgumentResolver{

  private final JwtService jwtService;



  @Override
  public boolean supportsParameter(MethodParameter parameter) {
    return parameter.hasParameterAnnotation(JwtUser.class);
  }

  @Override
  public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
                                WebDataBinderFactory binderFactory) {
    String token = webRequest.getHeader(AUTHORIZATION);
    User user = jwtService.decode(token);
    var annotation = parameter.getParameterAnnotation(JwtUser.class);

    if (annotation.required() && user == null) {
      throw new BadCredentialsException("user not auth");
    }
    return user;
  }
}
