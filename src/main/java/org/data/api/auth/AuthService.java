package org.data.api.auth;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.data.api.jwt.JwtService;
import org.data.api.profile.UserProfile;
import org.data.api.profile.UserProfileService;
import org.data.api.auth.request.LoginRequest;
import org.data.api.auth.request.UserRegistrationRequest;
import org.data.api.user.User;
import org.data.api.user.UserRepository;
import org.data.api.user.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
@Slf4j
public class AuthService {

  private final PasswordEncoder passwordEncoder;
  private final UserService userService;
  private final UserProfileService userProfileService;
  private final JwtService jwtService;
  private final UserRepository userRepository;

  public Map<String, String> registration(UserRegistrationRequest registration) {

    if (registration.getEmail() == null) {
      return Map.of("status", "error", "description", "mail must be not empty");
    }
    if (!Pattern.compile("^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))$").matcher(registration.getEmail()).matches()) {
      return Map.of("status", "error", "description", " not valid mail");
    }
    if (registration.getPassword() == null) {
      return Map.of("status", "error", "description", "pass must be not empty");
    }
    if (!Pattern.compile("(?=.*?[A-Z])(?=.*?[0-9]).{8,}").matcher(registration.getPassword()).matches()) {
      return Map.of("status", "error", "description", " not valid password");
    }
    var user = new User();
    user.setEmail(registration.getEmail());
    user.setPassword(passwordEncoder.encode(registration.getPassword()));
    User savedUser = userService.saveAndReturnUser(user);

    UserProfile profile = new UserProfile();
    profile.setUserId(savedUser.getId());
    profile.setAbout(registration.getAbout());
    profile.setBirthday(registration.getBirthday());
    profile.setNickname(registration.getNickname());
    userProfileService.save(profile);

    String token = jwtService.encode(savedUser);

    var response = new HashMap<>();
    response.put("id", user.getId());
    response.put("token", token);

    return Map.of(
      "status", "ok",
      "token", token
    );
  }

  public Object login(LoginRequest loginRequest) {

    var user = userRepository.getUserByEmail(loginRequest.getEmail());
    if (user != null) {
      passwordEncoder.matches(loginRequest.getPassword(), user.getPassword());
      String token = jwtService.encode(user);

      return Map.of(
        "status", "ok",
        "token", token
      );
    } else {
      log.info("User not found!");
    }
    return Map.of(
      "status", "error"
    );
  }
}
