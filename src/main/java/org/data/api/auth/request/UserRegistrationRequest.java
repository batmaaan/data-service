package org.data.api.auth.request;

import lombok.*;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationRequest {

  private String email;
  private String password;
  private Date birthday;
  private Date creationDate;
  private Date updateDate;
  private String nickname;
  private String about;
}
