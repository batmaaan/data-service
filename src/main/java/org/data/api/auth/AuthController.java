package org.data.api.auth;

import org.data.api.auth.request.LoginRequest;
import org.data.api.auth.request.UserRegistrationRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users/auth")
public class AuthController {

  private final AuthService userRegistrationService;

  public AuthController(AuthService service) {
    this.userRegistrationService = service;
  }

  @PostMapping(path = "/registration")
  public Object registration(@RequestBody UserRegistrationRequest registrationRequest) {
    return userRegistrationService.registration(registrationRequest);
  }

  @PostMapping(path = "/login")
  public Object login(@RequestBody LoginRequest loginRequest){
    return userRegistrationService.login(loginRequest);
  }

}
