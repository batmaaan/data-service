package org.data.api.news;


import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.data.api.annotations.JwtUser;
import org.data.api.news.request.CreateNewsRequest;
import org.data.api.news.request.UpdateNewsRequest;
import org.data.api.news.response.NewsResponse;
import org.data.api.user.User;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.data.api.utils.ApiUtil.AUTHORIZATION;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/v1/news")

public class NewsController {

  private final NewsService newsService;

  @GetMapping(path = "/id")

  public NewsResponse getNewsById(News news) {
    News newsForResponse = newsService.getNewsById(news.getId());
    return NewsResponse.fromNews(newsForResponse);
  }


  @GetMapping(path = "/list")
    public Map<String, Object> listNews(
    @RequestParam int offset,
    @RequestParam int limit,
    @RequestParam(defaultValue = "desc") String order) {
    return newsService.listNews(offset, limit, order);
  }

  @GetMapping(path = "/get10HotNews")
  public List<NewsResponse> get10HotNews() {
    List<NewsResponse> news = newsService.get10HotNews();
    List<NewsResponse> responses = news.stream()
      .map(p -> new NewsResponse(p.getId(), p.getUserId(), p.getTitle(), p.getDescription(), p.getCreationDate(), p.getUpdateDate()))
      .collect(Collectors.toList());
    return responses;
  }

  @SecurityRequirement(name = AUTHORIZATION)
  @PostMapping(path = "/create")
  public NewsResponse create(@JwtUser User user,
                             @RequestBody CreateNewsRequest request) {

    News news = new News();
    news.setUserId(user.getId());
    news.setTitle(request.getTitle());
    news.setDescription(request.getDescription());
    news.setCreationDate(new Date());
    news.setUpdateDate(new Date());

    return newsService.create(news);
  }

  @SecurityRequirement(name = AUTHORIZATION)
  @DeleteMapping(path = "/delete")
 public Map<String, String> delete(@RequestParam int id, @JwtUser User user) {
    try {
      newsService.delete(id, user.getId());
      return Map.of(
        "status", "ok"
      );
    } catch (Exception e) {
      log.error("error invalid user!!", e);
      return Map.of("status", "error"
      );
    }
  }

  @SecurityRequirement(name = AUTHORIZATION)
  @PostMapping("/update")
  public Map<String, Object> updateNews(@JwtUser User user,
                                        @RequestBody UpdateNewsRequest newsRequest) {

    try {
      NewsResponse response = newsService.updateNews(user.getId(), newsRequest);
      return Map.of(
        "status", "ok",
        "data", response
      );
    } catch (Exception e) {
      log.error("updateNews: ", e);
      return Map.of(
        "status", "error"
      );
    }
  }
  /*@PostMapping(path = "/savemillion")
  public NewsResponse saveMillion() {
    for (int i = 0; i < 1000000; i++) {
      var news = new News();
      news.setTitle(String.valueOf(i));
      news.setDescription(String.valueOf(i));
      news.setCreationDate(new Date());
      news.setUpdateDate(new Date());

    }
    return newsService.create(new News());
  }*/

}
