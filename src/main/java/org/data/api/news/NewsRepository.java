package org.data.api.news;

import lombok.AllArgsConstructor;
import org.data.api.news.response.NewsResponse;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
@AllArgsConstructor
public class NewsRepository {

  private final JdbcTemplate jdbcTemplate;


  public News getNewsById(Integer id) {
    News news = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from news where id = ?");
      statement.setInt(1, id);

      var resultSet = statement.executeQuery();

      var res = new News();
      if (resultSet.next()) {
        res.setId(resultSet.getInt("id"));
        res.setUserId(resultSet.getInt("user_id"));
        res.setCreationDate(resultSet.getDate("creation_date"));
        res.setUpdateDate(resultSet.getDate("update_date"));
        res.setTitle(resultSet.getString("title"));
        res.setDescription(resultSet.getString("description"));
      }
      return res;
    });
    return news;
  }

  public News getNewsByUserId(Integer userId) {
    News news = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from news where user_id = ?");
      statement.setInt(1, userId);

      var resultSet = statement.executeQuery();

      var res = new News();
      if (resultSet.next()) {
        res.setId(resultSet.getInt("id"));
        res.setUserId((resultSet.getInt("user_id")));
        res.setTitle(resultSet.getString("title"));
        res.setDescription(resultSet.getString("description"));
      }
      return res;
    });
    return news;
  }


  public List<News> getListNews(int offset, int limit, String order) {
    List<News> newsSet = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from news order by creation_date, ? limit ? offset ?");
      statement.setString(1, order);
      statement.setInt(2, limit);
      statement.setInt(3, offset);

      var newsList = statement.executeQuery();

      List<News> res = new ArrayList<>();

      while (newsList.next()) {
        res.add(News.builder()
          .id(newsList.getInt("id"))
          .userId(newsList.getInt("user_id"))
          .title(newsList.getString("title"))
          .description(newsList.getString("description"))
          .creationDate(newsList.getDate("creation_date"))
          .updateDate(newsList.getDate("update_date"))
          .build());
      }
      return res;


    });
    return newsSet;
  }

  public List<NewsResponse> get10HotNews() {
    List<NewsResponse> newsSet = jdbcTemplate.execute((Connection con) -> {
      var statement = con.prepareStatement("select * from news order by creation_date DESC limit 10 offset 0");
      var newsList = statement.executeQuery();

      List<NewsResponse> res = new ArrayList<>();

      while (newsList.next()) {
        res.add(NewsResponse.builder()
          .id(newsList.getInt("id"))
          .userId(newsList.getInt("user_id"))
          .title(newsList.getString("title"))
          .description(newsList.getString("description"))
          .creationDate(newsList.getDate("creation_date"))
          .updateDate(newsList.getDate("update_date"))
          .build());
      }
      return res;


    });
    return newsSet;
  }

  public News save(News news) {
    Integer id = jdbcTemplate.execute((Connection con) -> {
        PreparedStatement statement = con.prepareStatement("insert into news (user_id, title, description, creation_date, update_date) values (?, ?, ?, ?, ?) returning id;");
        statement.setInt(1, news.getUserId());
        statement.setString(2, news.getTitle());
        statement.setString(3, news.getDescription());
        statement.setDate(4, new java.sql.Date(news.getCreationDate().getTime()));
        statement.setDate(5, new java.sql.Date(news.getUpdateDate().getTime()));

        ResultSet res = statement.executeQuery();
        if (!res.next()) {
          return null;
        } else {
          return res.getInt("id");
        }
      }
    );
    news.setId(id);
    return news;
  }

  public News update(News news, Integer id) {
    jdbcTemplate.update("UPDATE news set title=?, description=?, update_date=? where id=?", news.getTitle(),
      news.getDescription(), news.getUpdateDate(), id);
    return news;
  }

  public void delete(int id) {
    jdbcTemplate.update("delete from news where id=?", id);
  }
}


