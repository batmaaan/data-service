package org.data.api.news.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.data.api.news.News;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NewsResponse {
  private Integer id;
  private Integer userId;
  private String title;
  private String description;
  private Date creationDate;
  private Date updateDate;

  public static NewsResponse fromNews(News news) {
    return NewsResponse.builder()
      .id(news.getId())
      .userId(news.getUserId())
      .title(news.getTitle())
      .description(news.getDescription())
      .creationDate(news.getCreationDate())
      .updateDate(news.getUpdateDate())
      .build();
  }
}
