drop table users;
create sequence users_id_seq;
create table users

(
  id           integer default nextval('users_id_seq'::regclass) not null
    primary key,
  email        varchar(100)                                      not null,
  password     varchar(100)                                      not null,
  creationDate timestamp without time zone,
  updateDate   timestamp with time zone
);

/*insert into users(id, email, password, creationDate, updateDate)
values (1, 'mail', 'pass', date '2022-02-11', date '2022-02-11');
insert into users(id, email, password, creationDate, updateDate)
values (2, 'mail', 'pass', date '2022-02-11', date '2022-02-11');
insert into users(id, email, password, creationDate, updateDate)
values (3, 'mail', 'pass', date '2022-02-11', date '2022-02-11');
insert into users(id, email, password, creationDate, updateDate)
values (4, 'mail', 'pass', date '2022-02-11', date '2022-02-11');

*/
