create sequence posts_id_seq;
create table posts
(
  id      integer default nextval('news_id_seq'::regclass) not null
    primary key,
  user_id integer                                          not null unique,
  text    varchar(30)                                      not null

);
