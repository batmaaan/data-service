drop table posts;
create table posts
(
  id      integer default nextval('news_id_seq'::regclass) not null
    primary key,
  user_id integer                                          not null unique,
  text    varchar(250)                                     not null

);
