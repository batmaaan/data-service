package org.data.api.repos;

import org.data.api.AbstractIntegrationTestForUser;
import org.data.api.user.User;
import org.data.api.user.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserRepositoryTest extends AbstractIntegrationTestForUser {


  @Autowired
  private UserRepository userRepository;

  @Test
  void getUserById() {
    User user = new User(null, "mail", "pass", new Date(), new Date(), new Date());
    userRepository.save(user);
    userRepository.getUserById(user.getId());
    assertEquals(user.getId(), user.getId());
    userRepository.delete(user.getId());
  }

  @Test
  void getUserList() {
    User user = new User(null, "mail", "pass", new Date(), new Date(), new Date());
    User user1 = new User(null, "mail", "pass", new Date(), new Date(), new Date());
    User user2 = new User(null, "mail", "pass", new Date(), new Date(), new Date());
    User savedUser = userRepository.save(user);
    User savedUser1 = userRepository.save(user1);
    User savedUser2 = userRepository.save(user2);

  }

  @Test
  void save() {
    User user = new User(null, "mail", "pass", new Date(), new Date(), new Date());
    User savedUser = userRepository.save(user);
    assertThat(savedUser).usingRecursiveComparison().ignoringFields("id").isEqualTo(user);
    userRepository.delete(savedUser.getId());
  }

  @Test
  void update() {
    User user = new User(null, "mail", "pass", new Date(), new Date(), new Date());
    User savedUser = userRepository.save(user);
    User buildUser = User.builder()
      .id(savedUser.getId())
      .email("mail_u")
      .password("pass_u")
      .creationDate(new Date())
      .updateDate(new Date())
      .birthday(new Date())
      .build();
    User updatedUser = userRepository.update(buildUser, savedUser.getId());
    assertThat(updatedUser).usingRecursiveComparison().ignoringFields("id").isEqualTo(buildUser);
    userRepository.delete(updatedUser.getId());
  }

  @Test
  void delete() {
    User user = new User(null, "mail", "pass", new Date(), new Date(), new Date());
    User savedUser = userRepository.save(user);
    userRepository.delete(savedUser.getId());
  }
}
