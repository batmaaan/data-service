package org.data.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.testcontainers.containers.PostgreSQLContainer;

@Slf4j
@SpringBootTest
@SpringJUnitWebConfig(
  initializers = AbstractIntegrationTestForNews.Initializer.class,
  classes = {ApiApplication.class}
)
public abstract class AbstractIntegrationTestForNews {


  public static final PostgreSQLContainer POSTGRES = new PostgreSQLContainer<>()
    .withInitScript("db/init_news.sql")
    //.withInitScript("db/init_users.sql")
    .withExposedPorts(5432);


  static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public void initialize(ConfigurableApplicationContext applicationContext) {
      POSTGRES.start();
      var testPropertyValues = TestPropertyValues.of(
        "app.datasource.url=" + POSTGRES.getJdbcUrl(),
        "app.datasource.username=" + POSTGRES.getUsername(),
        "app.datasource.password=" + POSTGRES.getPassword()
      );
      testPropertyValues.applyTo(applicationContext.getEnvironment());
    }
  }
}
